# Coding Standards #

These are the Drupal coding standards for our websites.

## Official Coding Standards ##

We follow the official coding standards outlined by [Drupal.org](https://www.drupal.org/coding-standards)
with a few exceptions, which we adopted from [PSR-2](http://www.php-fig.org/psr/psr-2/). We've also
adopted the [PEAR](http://www.php-fig.org/psr/psr-2/) standard for Docblocks.

### Related Articles ###

* [Drupal Object-Oriented Code Standards](https://www.drupal.org/node/608152)
* [Drupal JavaScript Code Standards](https://www.drupal.org/node/172169)

## Drupal Coding Standard Exceptions ##

The following is a list of exceptions that we have adopted from the PSR-2
coding standard.

### Overview ###

* Opening braces for classes MUST go on the next line, and closing braces MUST go on the next line after the body.
* Opening braces for methods MUST go on the next line, and closing braces MUST go on the next line after the body.
* Control structure keywords MUST have one space after them; method and function calls MUST NOT.
* All PHP files MUST use the Unix LF (linefeed) line ending.
* All PHP files MUST end with a single blank line.
* The closing ?> tag MUST be omitted from files containing only PHP.

### Docblocks ###

All docblocks MUST have the original author of the function. If you modify the
function you MUST add your name as an additional author.

Here is an example of a well-formed docblock:

```
/**
 * Short description goes here.
 *
 * @author     Original Author <author@example.com>
 * @author     Another Author <additional_author@example.com>
 * @since      File or method available since Release 1.2.
 * @deprecated Deprecated as of Release 2.0.
 * @see        my_function(), MyClass:my_function()
 * @version    1.0
 * @param      string $my_string An example argument with a description.
 * @return     string
 */
```

### Lines ###

* There MUST NOT be a hard limit on line length.
* The soft limit on line length MUST be 120 characters; automated style checkers MUST warn but MUST NOT error at the soft limit.
* Lines SHOULD NOT be longer than 80 characters; lines longer than that SHOULD be split into multiple subsequent lines of no more than 80 characters each.
* There MUST NOT be trailing whitespace at the end of non-blank lines.
* Blank lines MAY be added to improve readability and to indicate related blocks of code.
* There MUST NOT be more than one statement per line.

### Keywords and True/False/Null ###

* The PHP constants true, false, and null MUST be in lower case.

### Methods ###

Here is an example well-formed method.

```
<?php
public function sample_function($a, $b = null)
{
  if ($a === $b) {
      bar();
  } elseif ($a > $b) {
      $foo->bar($arg1);
  } else {
      BazClass::bar($arg2, $arg3);
  }
}

final public static function bar()
{
  // method body
}
```

Argument lists MAY be split across multiple lines, where each subsequent line is indented once. When doing so, the first item in the list MUST be on the next line, and there MUST be only one argument per line.

```
<?php
$foo->bar(
  $long_argument,
  $longer_argument,
  $much_longer_argument
);
```

### if, elseif, else ###

An if structure looks like the following. Note the placement of parentheses, spaces, and braces; and that else and elseif are on the same line as the closing brace from the earlier body.

```
<?php
if ($my_expr_1) {
  // if body
} elseif ($my_expr_2) {
  // elseif body
} else {
  // else body;
}
```

The keyword elseif SHOULD be used instead of else if so that all control keywords look like single words.

### Control Structures ###

* There MUST be one space after the control structure keyword.
* There MUST NOT be a space after the opening parenthesis.
* There MUST NOT be a space before the closing parenthesis.
* There MUST be one space between the closing parenthesis and the opening brace.
* The structure body MUST be indented once.
* The closing brace MUST be on the next line after the body.