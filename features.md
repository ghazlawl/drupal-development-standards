# Features #

### Creating a Feature ###

When creating a feature, use the following as a guide. Features should be generated in the directory ``sites/all/modules/custom/features`.

* **Name**: ISEN Events Feature
* **Description:** Contains the content type, taxonomies, and views for events.
* **Version:** 7.x-1.0
* **Dependencies:** ISEN Events (example module below)

### Companion Module ###

It is advised that you also create a custom module to handle any preprocess, alter, or other hooks specific to 
your feature that wouldn't be bundled with the theme. This example module would live in `sites/all/modules/custom/isen_events`.

* **Name:** Isen Events
* **Description:** Contains the events functionality for ISEN. Requires the ISEN Events feature.
* **Package:** Orbit Media Studios
* **Core:** 7.x-1.0

### Notes ###
* If you enhance or make changes to a feature, the feature should be regenerated, committed, and pushed to Bitbucket.
* Remember to increase the minor version number by one. So, `7.x-1.1` would become `7.x-1.2`.