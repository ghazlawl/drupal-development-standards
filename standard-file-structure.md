# Drupal File Structure #

Below is a directory tree of a typical Drupal project with descriptions of important children. Not all possible children are listed in this tree.

```
+-- sites
|   +-- all
|       +-- modules
|           +-- contrib -- All modules downloaded from Drupal.org go in this directory.
|           +-- custom -- All custom modules for the project go in this directory.
|               +-- [project_name]_core -- Create a custom module for your project and place it in this directory.
|                   +-- [project_name]_core.info
|                   +-- [project_name]_core.module
|       +-- themes
|           +-- bootstrap -- This is the parent Bootstrap theme that our child theme is built upon.
|           +-- orbitmedia -- This is the child theme for the project.
|               +-- inc -- Template-level includes go in this directory.
|               +-- resources
|                   +-- css -- Compiled CSS goes in this directory.
|                   +-- images -- Static images for the project go in this directory.
|                       +-- placeholder -- Placeholder images go in this directory.
|                   +-- js -- JavaScript goes in this directory.
|                   +-- scss -- Pre-compiled SCSS goes in this directory.
|                       +-- core -- Core-specific SCSS (Drupal, placeholders, mixins, etc) goes in this directory.
|                       +-- features -- Feature-specific SCSS goes in this directory.
|                           +-- _bios.scss
|                           +-- _news.scss
|                           +-- _resources.scss
|                           +-- ...
|                       +-- layout -- Layout-specific SCSS goes in this directory.
|                           +-- _footer.scss
|                           +-- _header.scss
|                           +-- _interior.scss
|                           +-- ...
|               +-- logo.png -- This is the logo of the project. Can be overridden in the UI.
|               +-- templates -- Drupal template overrides go in this directory.
|               +-- template.php -- Template-level Drupal hooks go in this file.
|               +-- theme-settings.php -- Theme-specific settings go in this file.
|   +-- default
|       +-- settings.php -- The database credentials are stored in this file.
|       +-- files -- File uploads go in this directory by default. This directory is publicly accessible.
```

## Where do I put my hooks? ##

If the hook is theme-specific, it should go in `sites/all/themes/[theme_name]/template.php`.

However, if the hook is site-specific, it should go in `sites/all/modules/custom/[project_name]_core/[project_name]_core.module`.

A quick test for this is to ask yourself, "If the client changes the theme for any reason, should this hook still trigger?" 
If the answer is NO, then the hook should go in the **template**. If the answer is YES, then the hook should go in a **module**.

## Which directory do I export my features to? ##

All features should be exported to `sites/all/modules/custom/features` to avoid conflicting with the contrib `Features` module.

## Which directory do I install modules to? ##

Custom modules should be installed to the `sites/all/modules/custom` directory. Contrib modules should be installed to the 
`sites/all/modules/contrib` directory. If a module is installed via the UI, it may be installed to the `sites/all/modules` directory. 
If any modules live in the `sites/all/modules` directory, please move them to the appropriate directory.

## Examples of feature-specific SCSS ##

These files are specific to functionality and should live in the `sites/all/themes/[theme_name]/resources/scss/features` directory.

* _account.scss
* _bios.scss
* _blog.scss
* _events.scss
* _locations.scss
* _news.scss
* _search.scss

## Examples of layout-specific SCSS ##

These files are specific to layout and should live in the `sites/all/themes/[theme_name]/resources/scss/layout` directory.

* _footer.scss
* _forms.scss
* _header.scss
* _home.scss
* _interior.scss
* _menus.scss
* _pageblocks.scss
* _sidebar.scss

## Examples of core SCSS ##

These files are part of the core SCSS and should live in the `sites/all/themes/[theme_name]/resources/scss/core` directory.

* _bootstrap.scss - `This is the original Bootstrap SCSS file.`
* _default.scss - `This file is loaded by layout.scss and the wysiwyg.scss.`
* _drupal.scss - `Contains overrides for tabs, etc.`
* _placeholders.scss - `Contains placeholders for use in other SCSS files.`
* _reset.scss - `Global resets of element styles.`
* _vars.scss - `Contains variables for use in other SCSS files.`