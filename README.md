# Drupal Development Standards #

This is a collection of our Drupal coding standards and standard Drupal file structure.

## Table of Contents ##

* [Coding Standards](https://bitbucket.org/ghazlawl/drupal-development-standards/src/master/coding-standards.md)
* [Standard File Structure](https://bitbucket.org/ghazlawl/drupal-development-standards/src/master/standard-file-structure.md)
* [Sublime Settings](https://bitbucket.org/ghazlawl/drupal-development-standards/src/master/sublime-settings.json)
